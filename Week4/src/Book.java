public class Book{
    private String Title,Author,Publish,Recommend;

    public String getTitle() {
        return Title;
    }
    public void setTitle(String Title)
    {
        this.Title = Title;
    }
    public String getAuthor()
    {
        return Author;
    }
    public void setAuthor(String Author)
    {
        this.Author = Author;
    }
    public String getPublish()
    {
        return Publish;
    }
    public void setPublish(String Publish)
    {
        this.Publish = Publish;
    }
    public String getRecommend()
    {
        return Recommend;
    }
    public void setRecommend(String Recommend)
    {
        this.Recommend = Recommend;
    }
    public String toString() {
        return "Title: " + Title + ", Author: " + Author + ", Published Date: " + Publish + ", Recommend?: " + Recommend;
    }
}
