import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JavaWithJson {
    public static String BookToJSON(Book book){
        ObjectMapper mapper = new ObjectMapper();
        String words = null;

        try {
            words = mapper.writeValueAsString(book);
        } catch (JsonProcessingException except) {
            System.err.println(except);
        }

        return words;
    }

    public static Book JSONToBook(String words) {

        ObjectMapper mapper = new ObjectMapper();
        Book book = null;

        try {
            book = mapper.readValue(words, Book.class);
        } catch (JsonProcessingException except) {
            System.err.println(except);
        }

        return book;
    }

    public static void main(String[] args) {

        Book B = new Book();
        B.setTitle("Mistborn");
        B.setAuthor("Brandon Sanderson");
        B.setPublish("2006");
        B.setRecommend("Yes");

        String json = BookToJSON(B);
        System.out.println(json);

        Book B2 = JSONToBook(json);
        System.out.println(B2);
    }
}
