package main;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


class JsonTest {

    @Test
    public void testA() {
        System.out.println("AssertSame Test");
        new Store();
        assertSame(7, Store.price((int) 10.0));
    }

    @Test
    public void testB() {
        System.out.println("AssertNotEquals Test");
        new Store();
        assertNotEquals(3, Store.Potential(13, 22));
    }

    @Test
    void testC() {
        System.out.println("AssertEquals Test");
        new Store();
        assertEquals(45, Store.Potential(3, 22));
    }

    @Test
    void testD() {
        System.out.println("AssertTrue Test");
        new Store();
        assertTrue(Store.inventory(13));
    }

    @Test
    void testE() {
        System.out.println("AssertFalse Test");
        new Store();
        assertFalse(Store.inventory(15));
    }
    @Test
    void testF() {
        System.out.println("AssertArrayEquals Test");
        String[] ProductNamesA = {"Rubix Cube", "Wii", "Pokemon Cards"};
        String[] ProductNamesB = {"Rubix Cube", "Wii", "Pokemon Cards"};

        new Store();
        assertArrayEquals(ProductNamesA, Store.newProducts(ProductNamesB));
    }

}
