public class Books {
    private final String author;
    private final String title;
    private final String release;

    public Books(String author, String title, String release) {
        this.author = author;
        this.title = title;
        this.release = release;
    }

    public String toString() {
        return "Author: " + author + ", Title: " + title + ", Year Released: " + release;
    }
}


