import java.util.*;
public class JavaCollections {
    public static void main(String[] args) {
        System.out.println("-- List --");
        List<String> list = new ArrayList<>();
        list.add("Meat");
        list.add("Fruit");
        list.add("Veggies");
        list.add("Dessert");
        for (String food : list)
            System.out.println(food);

        System.out.println("-- Queue --");
        Queue<String> queue = new PriorityQueue<>();
        queue.add("c");
        queue.add("b");
        queue.add("a");
        queue.add("C");
        queue.add("B");
        queue.add("A");
        Iterator<String> iterator = queue.iterator();
        while (iterator.hasNext())
            System.out.println(queue.poll());

        System.out.println("-- List using Generics --");
        List<Books> BookList = new LinkedList<>();
        BookList.add(new Books("Brandon Sanderson", "Mistborn", "2006"));
        BookList.add(new Books("Bradon Mull", "Fablehaven", "2006"));
        BookList.add(new Books("David Eddings", "The Belgariad", "1982"));

        for (Books book : BookList)
            System.out.println(book);

        System.out.println("-- Tree Set --");
        Set<String> FavAuthors = new TreeSet<>();

        FavAuthors.add("Brandon Sanderson");
        FavAuthors.add("Brandon Mull");
        FavAuthors.add("David Eddings");
        FavAuthors.add("Brandon Sanderson");

        System.out.println(FavAuthors);


    }
}