package Servlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServlet;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Servletview", urlPatterns = {"/Servletview"})
public class Servletlook extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String name = request.getParameter("name");
        String number = request.getParameter("number");
        String tool = request.getParameter("tool");

        PrintWriter out = response.getWriter();
        out.println("<h2>Thank you for renting with Rocknak's Hardware</h2>");
        out.println("<p>Your tool will be out shortly " + name + "</p>");
        out.println("<div><p>Number: " + number + "</p>");
        out.println("<p>Tool Rented: " + tool + "</p>");
        out.println("<a href=\"list\">Available Tools</a>");
        out.println("</body></html>");
    }
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        out.println("<html><head><style>div{border-radius:7px;background-color:#f2f2f2;padding:15px;}</style></head><body>");
        out.println("<h4>Something went wrong, we are fixing it now.</h4>");
        out.println("<div><p>Right now this is not available.</p></div>");
        out.println("</body></html>");
    }
}