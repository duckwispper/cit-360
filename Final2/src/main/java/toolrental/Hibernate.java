package toolrental;

import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.boot.MetadataSources;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;


public class Hibernate {

    private static final SessionFactory sessionFactory = createSessionFactory();
    private static SessionFactory createSessionFactory() {
        try {
            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                    .configure("hibernate.cfg.xml").build();

            Metadata data = new MetadataSources(serviceRegistry).getMetadataBuilder().build();

            return data.getSessionFactoryBuilder().build();
        } catch (Throwable ex) {

            System.err.println("SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

}