package toolrental;
import javax.persistence.*;
@Table(name = "Tools")
public class Rental {
    @Id
    @Column(name = "Info")
    int info;

    @Column(name = "Name")
    private String name;
    @Column(name = "Phone Number")
    private String number;
    @Column(name = "tool")
    private String tool;
    public Rental() {
    }
    public Rental(String name, String number, String tool) {
        this.name = name;
        this.number = number;
        this.tool = tool;
    }

    public int getInfo(int info) {
        return info;
    }
    public void setInfo(int info) {
        this.info = info;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getNumber() {
        return number;
    }
    public void setNumber(String Number) {
    }
    public String getTool() { return tool;
    }
    public void setTool(String tool) {
        this.tool = tool;
    }
    public String toString()
    {
        return  name + " " + number + " " + tool;
    }
}