package toolrental;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;

public class JSON {

    public static String userToJSON(List<Rental> p){
        ObjectMapper mapper = new ObjectMapper();
        String info = "";

        try {
            info = mapper.writeValueAsString(p);
        } catch (JsonProcessingException e) {
            System.err.println();
        }

        return info;
    }



}