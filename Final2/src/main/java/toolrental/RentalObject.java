package toolrental;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.ArrayList;
import java.util.List;

public class RentalObject {
    SessionFactory factory;
    Session session = null;

    private static toolrental.RentalObject t = null;

    public RentalObject()
    {
        factory = Hibernate.getSessionFactory();
    }

    public static toolrental.RentalObject getInstance()
    {
        if (t == null) {
            t = new toolrental.RentalObject();
        }

        return t;
    }


    public List<Rental> getRentalItem() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from toolrental.Rental";
            List<Rental> lr = (List<Rental>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return lr;

        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }
    public void addRentalDetails(String name, String number, String tool){
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            Rental rent = new Rental();
            rent.setTool(tool);
            rent.setName(name);
            rent.setNumber(number);
            session.save(rent);
            String sql = "from toolrental.Rental";
            session.createQuery(sql).getResultList();
            session.getTransaction().commit();

            System.out.println("-- List --");
            List<String> list = new ArrayList<>();
            list.add("Jigsaw");
            list.add("Chain Saw");
            list.add("Hammer-drills");
            list.add("Grinder");
            list.add("Drill");
            list.add("Sander");
            for (String Rentaltools : list)
                System.out.println(Rentaltools);

        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
        } finally {
            session.close();
        }
    }
}
