class Main implements Runnable {

    Thread threads;
    String packs;

    Main( String name) {
        packs = name;
        System.out.println("Opening Pack " +  packs + "." );

    }

    public void run() {
        System.out.println("Didn't find what you wanted in " +  packs + " keep opening.");
        try {
            for(int i = 1; i > 0; i--) {
                System.out.println("Opening Pack " + packs + "." );

                Thread.sleep(1000);
            }
        }catch (InterruptedException e) {
            System.out.println("Tore a card in" +  packs + " be more careful");
        }
        System.out.println("You found what you wanted in " +  packs + " finally!");
    }

    public void start () {
        System.out.println("Looking through " +  packs + "." );
        if (threads == null) {
            threads = new Thread (this, packs);
            threads.start ();
        }
    }
}