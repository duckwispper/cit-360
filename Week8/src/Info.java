import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Info {

    public static void main(String[] args) {
        ExecutorService myService = Executors.newFixedThreadPool(3);
        Main R1 = new Main("M20");
        Main R2 = new Main("Innistrad");
        Main R3 = new Main("Amonkhet");
        Main R4 = new Main("Kamigawa Neon Dynasty");
        Main R5 = new Main("Streets of New Capenna");
        Main R6 = new Main("Adventures in the Forgotten Realms");

        myService.execute(R1);
        myService.execute(R2);
        myService.execute(R3);
        myService.execute(R4);
        myService.execute(R5);
        myService.execute(R6);

        myService.shutdown();

    }

}
