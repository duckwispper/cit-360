import java.util.*;
public class ExceptionHandling {
    public static void main(String[] args) {

            loop();
    }
    public static void loop(){
        Scanner scan = new Scanner(System.in);
        try {
            int num1, num2, sum;
            System.out.println("Please Enter First Number: ");
            num1 = scan.nextInt();

            System.out.println("Please Enter Second Number: ");
            num2 = scan.nextInt();

            sum = num1 / num2;
            System.out.println("Quotient of these numbers: " + sum);
        } catch (ArithmeticException e) {

            System.out.println("Don't divide a number by zero");
            loop();

        }
    }
}


